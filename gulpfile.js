var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    sassGlob = require('gulp-sass-glob'),
    sassMerge = require('gulp-merge-media-queries'),
    autoprefixer = require('gulp-autoprefixer'),
    cssmin = require('gulp-cssmin'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    fileinclude = require('gulp-file-include');

var browserSync = require('browser-sync').create();

var sassSources = ['static/scss/**/*.scss'],
    jsApp = ['static/js/app/*.js'],
    jsPlugins = ['static/js/plugins/*.js'],
    htmlSources = ['templates/**/*.html'];

// Sass
gulp.task('sass', function() {
    return gulp
        .src('./static/scss/main.scss')
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(sassMerge())
        .pipe(autoprefixer({ browsers: ['last 2 versions'] }))
        .pipe(cssmin())
        .pipe(gulp.dest('./static/css'))
        .pipe(browserSync.stream());
});

gulp.task('app', function() {
  return gulp.src('./static/js/app/*.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./static/js'));
});

gulp.task('plugins', function() {
  return gulp.src('./static/js/plugins/*.js')
    .pipe(concat('plugins.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./static/js'));
});

// File Includes
gulp.task('fileinclude', function() {
    return gulp
        .src(['templates/pages/*.html'])
        .pipe(plumber())
        .pipe( fileinclude({ prefix: '@@', basepath: 'templates/modules' }) )
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream());
});

// BrowserSync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: true
    });
});

/* ------------------------------------------ Static Projects */

// Watch 
gulp.task('watch', function() {
    gulp.watch(sassSources, ['sass']);
    gulp.watch(jsApp, ['app']);
    gulp.watch(jsPlugins, ['plugins']);
    gulp.watch(htmlSources, ['fileinclude']);
});

gulp.task('default', ['sass','app','plugins','fileinclude','browser-sync','watch']);