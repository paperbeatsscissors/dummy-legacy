//------------------------------------------------- PLUGINS
var app;

(function($){
    app = {
        //------------------------------------------------- ON READY
        init: function() {

        },
        //------------------------------------------------- PLUGINS
        plugins: function() {

        },
        //------------------------------------------------- ON SCROLL
        scrollEvents: function() {
        }
    };

    $(document).ready(function(){
        app.init();
        app.plugins();

        /*
        $(window).on('scroll', function(){
            app.scrollEvents();
        }).scroll();
        */
    });
})(jQuery);